/*
 * Copyright (c) 2014.
 * Created by Tiberiu Mihai
 * Email: tiberiu.mihai@gmail.com
 * Phone: +40 748 471 512
 */

package model;

public class Category {
    private String type;

    public Category(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override public int hashCode() {
        int prime = 31;
        return prime *
               this.type.hashCode();
    }

    @Override public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (!(object instanceof Category)) {
            return false;
        }
        Category category = (Category) object;
        if (this.type.equals(category.getType())) {
            return true;
        }
        return false;
    }
}