/*
 * Copyright (c) 2014.
 * Created by Tiberiu Mihai
 * Email: tiberiu.mihai@gmail.com
 * Phone: +40 748 471 512
 */

package model;

import java.util.Calendar;
import java.util.TimeZone;
import java.util.UUID;

public class Transaction {

    private UUID uuid;
    private String name;
    private Category category;
    private Double value;
    private Long timestamp;

    protected static TimeZone UTC_TIMEZONE = TimeZone.getTimeZone("UTC");

    public Transaction(String name, Category category, double value) {
        // make sure this is another transaction
        // even if it has the same category and the same value
        this.uuid = UUID.randomUUID();

        this.name = name;
        this.category = category;
        this.value = value;


        Calendar UTC_CALENDAR = Calendar.getInstance(UTC_TIMEZONE);
        this.timestamp = UTC_CALENDAR.getTime().getTime();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCategory(Category category) {
        return this.category.equals(category);
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    @Override public int hashCode() {
        int prime = 31;
        return prime *
               this.name.hashCode() *
               this.category.hashCode() *
               this.value.hashCode() *
               this.uuid.hashCode() *
               this.timestamp.hashCode();
    }

    @Override public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (!(object instanceof Transaction)) {
            return false;
        }
        Transaction transaction = (Transaction) object;
        if (this.uuid.equals(transaction.getUuid()) &&
            this.name.equals(transaction.getName()) &&
            this.category.equals(transaction.getCategory()) &&
            this.value.equals(transaction.getValue()) &&
            this.timestamp.equals(transaction.getTimestamp())) {
            return true;
        }
        return false;
    }
}
