/*
 * Copyright (c) 2014.
 * Created by Tiberiu Mihai
 * Email: tiberiu.mihai@gmail.com
 * Phone: +40 748 471 512
 */

package model;

import java.util.ArrayList;
import java.util.List;

public class Account {
    private List<Expense> expenses = new ArrayList<Expense>();
    private List<Income> incomes = new ArrayList<Income>();

    public List<Expense> getExpenses() {
        return expenses;
    }

    public void addExpense(Expense expense) {
        this.expenses.add(expense);
    }

    public void removeExpense(Expense expense) {
        this.expenses.remove(expense);
    }

    public List<Income> getIncomes() {
        return incomes;
    }

    public void addIncome(Income income) {
        this.incomes.add(income);
    }

    public void removeIncome(Income income) {
        this.incomes.remove(income);
    }
}
