/*
 * Copyright (c) 2014.
 * Created by Tiberiu Mihai
 * Email: tiberiu.mihai@gmail.com
 * Phone: +40 748 471 512
 */

package model;

public class Income extends Transaction {

    public Income(String name, Category category, double value) {
        super(name, category, value);
    }

}
