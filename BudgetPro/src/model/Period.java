/*
 * Copyright (c) 2014.
 * Created by Tiberiu Mihai
 * Email: tiberiu.mihai@gmail.com
 * Phone: +40 748 471 512
 */

package model;

/**
 * Created by tibi on 03/11/14.
 */
public class Period {
    private long fromTimestamp;
    private long toTimestamp;

    public Period(long fromTimestamp, long toTimestamp) {
        this.fromTimestamp = fromTimestamp;
        this.toTimestamp = toTimestamp;
    }

    public long getFromTimestamp() {
        return fromTimestamp;
    }

    public void setFromTimestamp(long fromTimestamp) {
        this.fromTimestamp = fromTimestamp;
    }

    public long getToTimestamp() {
        return toTimestamp;
    }

    public void setToTimestamp(long toTimestamp) {
        this.toTimestamp = toTimestamp;
    }
}
