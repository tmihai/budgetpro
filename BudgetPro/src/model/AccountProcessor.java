/*
 * Copyright (c) 2014.
 * Created by Tiberiu Mihai
 * Email: tiberiu.mihai@gmail.com
 * Phone: +40 748 471 512
 */

/*
TODO
[x] balance - the difference between the income and the sum of expenses

[x] top_expense displaying the most expensive expense
[x] expenses_average - the expenses average
[ ] less_expensive_category - the category which has the minimum average expenses
[ ] top_expensive_category - the category with the maximum average
 */

package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AccountProcessor {
    private AccountProcessor() {}

    public static List<Transaction> getTransactions(Account... accounts) {
        List<Transaction> transactions = new ArrayList<Transaction>();
        for (Account account : accounts) {
            transactions.addAll(account.getExpenses());
            transactions.addAll(account.getIncomes());
        }
        // add sorting
        Collections.sort(transactions, new Comparator<Transaction>() {
            @Override
            public int compare(Transaction transaction, Transaction transaction2) {
                return transaction.getTimestamp().compareTo(transaction2.getTimestamp());
            }
        });
        return transactions;
    }

    public static List<Transaction> getTransactions(Period period, Account... accounts) {
        List<Transaction> transactions = new ArrayList<Transaction>();
        for (Account account : accounts) {
            for (Expense expense : account.getExpenses()) {
                if (expense.getTimestamp() >  period.getFromTimestamp() &&
                    expense.getTimestamp() <= period.getToTimestamp()) {
                    transactions.add(expense);
                }
            }
            for (Income income : account.getIncomes()) {
                if (income.getTimestamp() >  period.getFromTimestamp() &&
                    income.getTimestamp() <= period.getToTimestamp()) {
                    transactions.add(income);
                }
            }
        }
        // add sorting
        Collections.sort(transactions, new Comparator<Transaction>() {
            @Override
            public int compare(Transaction transaction, Transaction transaction2) {
                return transaction.getTimestamp().compareTo(transaction2.getTimestamp());
            }
        });
        return transactions;
    }

    public static double getExpensesTotalValue(Account... accounts) {
        double expensesValue = 0.0;
        for (Account account : accounts) {
            for (Expense expense : account.getExpenses()) {
                expensesValue += expense.getValue();
            }
        }
        return expensesValue;
    }

    public static double getIncomesTotalValue(Account... accounts) {
        double incomesValue = 0.0;
        for (Account account : accounts) {
            for (Income income : account.getIncomes()) {
                incomesValue += income.getValue();
            }
        }
        return incomesValue;
    }

    public static double getBalance(Account... accounts) {
        double incomesValue = getIncomesTotalValue(accounts);
        double expensesValue = getExpensesTotalValue(accounts);
        double balance = incomesValue - expensesValue;
        return balance;
    }

    public static Expense getTopExpense(Account... accounts) {
        Expense topExpense = new Expense("", new Category(""), 0.0);
        for (Account account : accounts) {
            for (Expense expense : account.getExpenses()) {
                if (topExpense.getValue() < expense.getValue()) {
                    topExpense = expense;
                }
            }
        }
        return  topExpense;
    }

    public static double getExpensesAverage(Account... accounts) {
        double expensesTotal = 0.0;
        double expensesCount = 0;
        for (Account account : accounts) {
            for (Expense expense : account.getExpenses()) {
                expensesTotal += expense.getValue();
            }
            expensesCount += account.getExpenses().size();
        }
        return  expensesTotal / expensesCount;
    }

    public static Category getLessExpensiveCategory(Account... accounts) {
        Expense lessExpense = null;
        for (Account account : accounts) {
            for (Expense expense : account.getExpenses()) {
                if (lessExpense == null) {
                    lessExpense = expense;
                } else if (lessExpense.getValue() > expense.getValue()) {
                    lessExpense = expense;
                }
            }
        }
        return lessExpense.getCategory();
    }

    public static Category getTopExpensiveCategory(Account... accounts) {
        Expense topExpense = getTopExpense(accounts);
        return topExpense.getCategory();
    }
}