/*
 * Copyright (c) 2014.
 * Created by Tiberiu Mihai
 * Email: tiberiu.mihai@gmail.com
 * Phone: +40 748 471 512
 */

import model.*;

import java.util.List;

public class BudgetPro {
    public static void main(String args[]) throws InterruptedException {

        // Account 1
        Account account = new Account();

        Income salary = new Income("Salary", new Category("Salary"), 5125);
        account.addIncome(salary);
        Thread.sleep(1000);

        Expense expense = new Expense("MacBook Pro", new Category("Computers"), 2500);
        account.addExpense(expense);
        Thread.sleep(1000);

        expense = new Expense("MacBook Pro", new Category("Computers"), 2500);
        account.addExpense(expense);
        Thread.sleep(1000);


        // Account 2
        Account account2 = new Account();

        Income bank = new Income("Bank", new Category("Bank income"), 500);
        account2.addIncome(bank);
        Thread.sleep(1000);

        // List all Transactions
        List<Transaction> transactions = AccountProcessor.getTransactions(new Account[] { account, account2 } );
        for (Transaction transaction : transactions) {
            String sign = transaction instanceof Expense ? " - " : " + ";
            System.out.println(transaction.getName() + " :: " + transaction.getCategory().getType());
            System.out.println(sign + transaction.getValue());
            System.out.println();
        }

        // List all Transactions in Period
        // Period period = new Period(long, long);
        // List<Transaction> transactions = AccountProcessor.getTransactions(period, new Account[] { account, account2 } );


        // Printout the balance for both accounts
        System.out.println("Balance:                 " + AccountProcessor.getBalance(new Account[] { account, account2 } ));
        System.out.println("Top expense:             " + AccountProcessor.getTopExpense(new Account[] { account, account2 } ).getName());
        System.out.println("Expenses average:        " + AccountProcessor.getExpensesAverage(new Account[]{account, account2}));
        System.out.println("Less expensive category: " + AccountProcessor.getLessExpensiveCategory(new Account[]{account, account2}).getType());
        System.out.println("Top expensive category:  " + AccountProcessor.getTopExpensiveCategory(new Account[]{account, account2}).getType());
    }
}